Hi and welcome!

### **About Stratego**
Stratego is a strategy board game for two players (single or doubles) on a
board of 10*10 game.

**Description**

The goal of this game is Capture the opposing team's flag or remove all movable pieces of the game.

### **How Install and Run**
Clone the project, open directory "untitled/src/main/java" . For Running run the main function in the Main class.

### **How play**
You can read rules everything about the Stratego game on the internet.

To move in the program, you must enter the number corresponding to the desired option and then press Enter.

For example :
    
    1_ New Game
    2_ Exit
I want to move into Login ,Just need to enter 1 and press Enter.

For playing new game you must enter the position of your pieces, You can enter the input manually or enter the address of a ".txt" file
(There is a file in the Stratego file with name of Pieces.txt for convenience).

example of enter :

    Scout,1,1

example for move the pieces :

    Major,2,3-u

'u' for up , 'd' for down , 'r' for right , 'l' for left

and for moving Scout piece :

    Scout,1,1-3,1

1,1 -> The coordinates of where the scout is located 

3,1 -> The coordinates of where the scout wants to go