import java.util.ArrayList;
import java.util.Collections;

public class AI {
    //Artificial intelligence

    static void computerPlay (int[][] table) {
        int col = 0;
        int i = 9;

        while (true) {
            ArrayList<Integer> arr = new ArrayList<>();
            for (int j = 0; j < 10 ; j++) {
                if (table[i][j] < 0 && table[i][j] >= -10) {
                    if (isMovable(table , i , j , 'u') || isMovable(table ,i ,j ,'r') ||
                    isMovable(table,i ,j ,'l') || isMovable(table ,i ,j ,'d')) {
                        arr.add(j);
                    }
                }
            }

            if (arr.size() < 1) {
                i--;
                continue;
            }

            Collections.shuffle(arr);
            col = arr.get(0);

            // first up then right, left, down
            char where = 'd';
            if (isMovable(table, i, col, 'd')) {
                where = 'd';
            } else if (isMovable(table, i, col, 'r')) {
                where = 'r';
            } else if (isMovable(table, i, col, 'l')) {
                where = 'l';
            } else if (isMovable(table, i, col, 'u')) {
                where = 'u';
            }

            Move.move(table, i, col, where);
            break;
        }
    }

    public static String message(int code)
    {
        String messages = "";
        switch (code){
            case 1 :
                messages = "Island! Can't go there!" ;
            case 2 :
                messages = "Out of range!" ;
            case 3 :
                messages = "Your own piece! Can't go there! " ;
            case 4 :
                messages = "Moved!" ;
            case 5 :
                messages = "Equal! Both gone!" ;
            case 6 :
                messages = "Attack!" ;
            case 7 :
                //messages = "Oops! You're out! " ;
                messages = "";
        }

        return messages ;
    }

    static boolean isMovable (int[][] table , int row , int col ,  char direction)
    {
        boolean res = false ;

        if (table[row][col] == 11 || table[row][col] == 12) {
            return false;
        }

        switch (direction)
        {
            case 'u':
                if (row > 0)
                {
                    if( table[row-1][col] == 13)  // 13-> island
                    {
                        System.out.println(message(1));
                    }
                    else if (table[row-1][col] >= -12 && table[row-1][col] <= -1)  // own pieces
                    {
                        System.out.println(message(3));
                    }
                    else {
                        res = true ;
                        System.out.println(message(4));
                    }
                }
                else if (row == 0) {  //out of range
                    System.out.println(message(2));
                }

                break;
            case 'd' :
                if (row < 9)
                {
                    if (table[row+1][col] == 13 ) //islands
                    {
                        System.out.println(message(1));
                    }
                    else if (table[row+1][col] >= -12 & table[row+1][col] <= -1) //own pieces
                    {
                        System.out.println(message(3));
                    }
                    else {
                        res = true ;
                        System.out.println(message(4));
                    }
                }
                else if (row == 9)
                {
                    System.out.println(message(2));
                }

                break;
            case 'l' :
                if (col > 0)
                {
                    if (table[row][col-1] == 13 ) //islands
                    {
                        System.out.println(message(1));
                    }
                    else if (table[row][col] >= -12 & table[row][col-1] <= -1) //own pieces
                    {
                        System.out.println(message(3));
                    }
                    else {
                        res = true ;
                        System.out.println(message(4));
                    }
                }
                else if (col == 0)
                {
                    System.out.println(message(2));
                }

                break;
            case 'r' :
                if (col < 9 )
                {
                    if (table[row][col+1] == 13 ) //islands
                    {
                        System.out.println(message(1));
                    }
                    else if (table[row][col+1] >= -12 & table[row][col+1] <= -1) //own pieces
                    {
                        System.out.println(message(3));
                    }
                    else {
                        res = true ;
                        System.out.println(message(4));
                    }
                }
                else if (col == 9)
                {
                    System.out.println(message(2));
                }

                break;
        }
        return res ;
    }
}
