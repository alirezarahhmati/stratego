import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

        while (true) {
            System.out.println("\n\n");
            System.out.println("""
                    1_ New Game\s
                    2_ Exit  \s
                    """);

            String s = input.next();
            switch (s.charAt(0)) {
                case '1' :
                    setColor ();
                    break;
                case '2' :
                    return;
            }
        }
    }

    private static void setColor() {
        Scanner input = new Scanner(System.in);
        Game game = new Game();

        while (true) {
            System.out.println("\n\n");
            System.out.println("""
                    1_ Blue \s
                    2_ Red  \s
                    3_ Back \s
                    """);

            String s = input.next();
            switch (s.charAt(0)) {
                case '1' :
                    game.setTurn(false);
                    setTable (game);
                    break;
                case '2' :
                    game.setTurn(true);
                    setTable (game);
                    break;
                case '3' :
                    return;
            }
        }
    }

    static void setTable (Game game) {
        Scanner input = new Scanner(System.in);

        while (true) {
            System.out.println("""
                    \n
                     \s
                    Place the pieces. \s
                    You have : \s
                    Six Bombs , a Marshal , a General , two Colonel , three Major , four Lieutenant , four Sergeant , five Miner , eight Scout , a Spy and a Flag \s
                    1_ Manual import \s
                    2_ Read from file \s
                    3_ Back \s
                    """);

            boolean loginToGame = false;
            String s = input.next();
            switch (s.charAt(0)) {
                case '1':
                    manualImport(game.getTable());
                    loginToGame = true;
                    break;
                case '2':
                    if (readFile(game.getTable())) {
                        loginToGame = true;
                    }
                    break;
                case '3' :
                    return;
            }


            if (loginToGame && checkPlacePieces (game.getTable())) {
                Play play = new Play();
                play.playGame(game);
            } else {
                System.err.print("your input was not correct!");
            }
        }
    }

    static void manualImport (int[][] table) {
        Scanner input = new Scanner(System.in);

        for (int i = 0; i < 40;) {
            System.out.println("Enter : ");
            String data = input.nextLine();
            try {
                placePieces(table, data);
                i++;
            } catch (Exception e) {
                System.out.println("Wrong input!");
            }
        }
    }

    static boolean readFile (int[][] table) {
        Scanner input = new Scanner(System.in);

        System.out.println("\n\n");
        System.out.println("Enter path : ");
        String path = input.next();

        File myFile = new File(path);
        try {
            Scanner myReader = new Scanner(myFile);
            for (int i = 0; i < 40; i++) {
                String data = myReader.nextLine();
                placePieces (table , data);
            }
            myReader.close();
        } catch (FileNotFoundException e) {
            System.out.println("An error occurred!");
            System.out.println("We can not open file.");
            System.out.println("Press any key to continue!");
            input.next();
            return false;
        }
        return true;
    }

    private static void placePieces(int[][] table, String data) {
        StringBuilder nut = new StringBuilder();
        StringBuilder row = new StringBuilder();
        StringBuilder col = new StringBuilder();
        int len = data.length();
        for (int i = 0; i < len; i++) {
            if (data.charAt(i) == ',') {
                for (i = i + 1; i < i + 3; i++) {
                    if (data.charAt(i) == ',') {
                        for (i = i + 1; i < len; i++) {
                            col.append(data.charAt(i));
                        }
                        break;
                    }
                    row.append(data.charAt(i));
                }
                break;
            }
            nut.append(data.charAt(i));
        }

        int nutInt = givPower(nut.toString());
        int rowInt = Integer.parseInt(row.toString());
        int colInt = Integer.parseInt(col.toString());
        table[10 - rowInt][colInt - 1] = nutInt;
    }

    static int givPower (String nut) {
        return switch (nut) {
            case "Scout" -> 2;
            case "Bomb" -> 11;
            case "Miner" -> 3;
            case "Sergeant" -> 4;
            case "Lieutenant" -> 5;
            case "Captain" -> 6;
            case "Major" -> 7;
            case "Colonel" -> 8;
            case "General" -> 9;
            case "Marshal" -> 10;
            case "Spy" -> 1;
            case "Flag" -> 12;
            default -> 0;
        };
    }

    private static boolean checkPlacePieces (int[][] table) {
        int bomb = 0, marshal = 0, general = 0, colonel = 0, major = 0, captain = 0, lieutenant = 0, sergeant = 0, miner = 0, scout = 0, spy = 0, flag = 0;

        for (int i = 6; i < 10; i++) {
            for (int j = 0; j < 10; j++) {
                switch (table[i][j]) {
                    case 2 -> scout++;
                    case 11 -> bomb++;
                    case 3 -> miner++;
                    case 4 -> sergeant++;
                    case 5 -> lieutenant++;
                    case 6 -> captain++;
                    case 7 -> major++;
                    case 8 -> colonel++;
                    case 9 -> general++;
                    case 10 -> marshal++;
                    case 1 -> spy++;
                    case 12 -> flag++;
                }
            }
        }

        return bomb == 6 && marshal == 1 && general == 1 && colonel == 2 && major == 3 && captain == 4 && lieutenant == 4 &&
                sergeant == 4 && miner == 5 && scout == 8 && spy == 1 && flag == 1;
    }
}
