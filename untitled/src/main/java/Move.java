
public class Move {


    static boolean isMovable (int[][] table , int row , int col ,  char direction)
    {
        boolean res = false ;

        if (table[row][col] == 11 || table[row][col] == 12) {
            return false;
        }

        switch (direction)
        {
            case 'u':
                if (row > 0)
                {
                    if( table[row-1][col] == 13)  // 13-> island
                    {
                        System.out.println(message(1));
                    }
                    else if (table[row-1][col] <= 12 && table[row-1][col] >= 1)  // own pieces
                    {
                        System.out.println(message(3));
                    }
                    else {
                        res = true ;
                        System.out.println(message(4));
                    }
                }
                else {  //out of range
                    System.out.println(message(2));
                }

                break;
            case 'd' :
                if (row < 9)
                {
                    if (table[row+1][col] == 13 ) //islands
                    {
                        System.out.println(message(1));
                    }
                    else if (table[row+1][col] <= 12 & table[row+1][col] >= 1) //own pieces
                    {
                        System.out.println(message(3));
                    }
                    else {
                        res = true ;
                        System.out.println(message(4));
                    }
                }
                else if (row == 9)
                {
                    System.out.println(message(2));
                }

                break;
            case 'l' :
                if (col > 0)
                {
                    if (table[row][col-1] == 13 ) //islands
                    {
                        System.out.println(message(1));
                    }
                    else if (table[row][col] <= 12 & table[row][col-1] >= 1) //own pieces
                    {
                        System.out.println(message(3));
                    }
                    else {
                        res = true ;
                        System.out.println(message(4));
                    }
                }
                else {
                    System.out.println(message(2));
                }

                break;
            case 'r' :
                if (col < 9 )
                {
                    if (table[row][col+1] == 13 ) //islands
                    {
                        System.out.println(message(1));
                    }
                    else if (table[row][col+1] <= 12 & table[row][col+1] >= 1) //own pieces
                    {
                        System.out.println(message(3));
                    }
                    else {
                        res = true ;
                        System.out.println(message(4));
                    }
                }
                else if (col == 9)
                {
                    System.out.println(message(2));
                }

                break;
        }
        return res ;
    }



    public static String message(int code)
    {
        String messages = "";
        switch (code){
            case 1 :
                messages = "Island! Can't go there!" ;
            case 2 :
                messages = "Out of range!" ;
            case 3 :
                messages = "Your own piece! Can't go there! " ;
            case 4 :
                messages = "Moved!" ;
            case 5 :
                messages = "Equal! Both gone!" ;
            case 6 :
                messages = "Attack!" ;
            case 7 :
                messages = "Oops! You're out! " ;
        }

        return messages ;
    }

    static void move(int[][] table , int row , int col , char direction ) {
        int r = row;
        int c = col;

        switch (direction) {
            case 'u' -> r = row - 1;
            case 'd' -> r = row + 1;
            case 'l' -> c = col - 1;
            case 'r' -> c = col + 1;
        }

        if (Math.abs(table[r][c]) == Math.abs(table[row][col])) {
            System.out.println(message(5));

            //delete both pieces
            table[r][c] = 0;
            table[row][col] = 0;
        }

        else if (Math.abs(table[r][c]) < Math.abs(table[row][col])) {
            if (table[r][c] == 0)  //if empty
            {
                System.out.println(message(4));
                table[r][c] = table[row][col];
                table[row][col] = 0;
            }
            else { //attack
                System.out.println(message(6));
                System.out.println(getPiece(table[row][col]) + " hit " + getPiece(table[r][c]));

                table[r][c] = table[row][col] ;  //replaced
                table[row][col] = 0 ; //moved -> empty
            }
        }
        else if (Math.abs(table[r][c]) > Math.abs(table[row][col]))
        {
            System.out.println(message(7));
            System.out.println(getPiece((-table[r][c])) + " hit " + getPiece(table[row][col]));

            table[row][col] = 0; //out -> deleted
        }

    }


    static String getPiece(int n){
        return switch (n) {
            case 2 -> "Scout";
            case 11-> "Bomb";
            case 3 -> "Miner";
            case 4 -> "Sergeant";
            case 5 -> "Lieutenant";
            case 6 -> "Captain";
            case 7 -> "Major" ;
            case 8 -> "Colonel" ;
            case 9 -> "General" ;
            case 10-> "Marshal" ;
            case 1 -> "Spy" ;
            case 12-> "Flag" ;
            default -> " ";
        };
    }

    static void scoutMove(int[][] table, int row, int col, int targetRow, int targetCol) {

        if (targetRow - 1 == row && targetCol == col) {
            System.out.println("1");
            move(table ,row ,col ,'d');
        }else if (targetRow + 1 == row && targetCol == col) {
            System.out.println("2");
            move(table ,row ,col ,'u');
        }else if (targetCol - 1 == col && targetRow == row) {
            System.out.println("3");
            move(table ,row ,col ,'r');
        }else if (targetCol + 1 == col && targetRow == row) {
            System.out.println("4");
            move(table ,row ,col ,'l');
        }else {
            table[targetRow][targetCol] = 2;
            table[row][col] = 0;
        }

    }

    static boolean isScoutMovable(int[][] table, int row, int col, int targetRow, int targetCol) { // (table ,4 ,1 ,5 ,1)

        boolean res = true;
        if (targetRow <= 9 & targetRow>=0 & targetCol<= 9 & targetCol>=0) {
            if (!ScoutDirection(row, col, targetRow, targetCol).equals("invalid")) {
                if (table[targetRow][targetCol] == 13)  // 13-> island
                {
                    System.out.println(message(1));
                    res = false;
                } else if (table[targetRow][targetCol] <= 12 & table[targetRow][targetCol] >= 1)  // own pieces
                {
                    System.out.println(message(3));
                    res = false;

                } else if (ScoutDirection(row, col, targetRow, targetCol).equals("vertical")) //island/own piece on the way
                {
                    if (targetRow < row) //check up or down vertical move
                    {
                        for (int i = 1; i <= (row - targetRow); i++) {
                            if (table[row - i][col] >= 1 & table[row - i][col] <= 13) {
                                System.out.println(message(9));
                                res = false;
                            }
                        }
                    } else {
                        for (int i = 1; i <= (targetRow - row); i++) {
                            if (table[row + i][col] >= 1 & table[row + i][col] <= 13) {
                                System.out.println(message(9));
                                res = false;
                            }
                        }
                    }
                }
                // check right or left  horizontal move
                else if (ScoutDirection(row, col, targetRow, targetCol).equals("horizontal")) {
                    if (targetCol < col) { //goes left
                        for (int j = 1; j <= (col - targetCol); j++) {
                            if (table[row][col - j] >= 1 & table[row][col - j] <= 13) {
                                System.out.println(message(9));
                                res = false;
                            }
                        }
                    } else { //goes right
                        for (int j = 1; j <= (targetCol - col); j++) {
                            if (table[row][col + j] >= 1 & table[row][col + j] <= 13) {
                                System.out.println(message(9));
                                res = false;
                            }
                        }
                    }
                }
            }
            else { //invalid target row and column
                res = false;
            }
        }
        else {
            res = false ;
        }

        return res;
    }

    public static String ScoutDirection(int row, int col, int targetRow, int targetCol)
    {
        String direction = "" ;
        if (row == targetRow)
        {
            if (targetCol == col )
            {
                direction = "same" ;
            }
            else {
                direction = "horizontal" ;
            }
        }
        else if (targetCol == col)
        {
            direction = "vertical" ;
        }
        else {
            direction = "invalid" ;
        }
        return direction ;
    }
}