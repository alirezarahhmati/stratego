import java.util.ArrayList;
import java.util.Collections;

public class Game {
    private int[][] table;
    private boolean isTurn;

    public Game () {
        table = new int[10][10];

        table[4][2] = table[4][3] = table[4][6] = table[4][7] = 13;
        table[5][2] = table[5][3] = table[5][6] = table[5][7] = 13;

        ArrayList<Integer> arr = new ArrayList<>();
        setShuffleArr (arr);
        Collections.shuffle(arr);
        int z = 0;
        for (int i = 0; i < 4; i++) {
            for (int j = 0; j < 10; j++) {
                table[i][j] = arr.get(z++);
            }
        }
    }

    private void setShuffleArr(ArrayList<Integer> arr) {
        for (int i = 0; i < 6; i++) {
            arr.add(-11);
        }
        arr.add(-10);
        arr.add(-9);
        for (int i = 0; i < 2; i++) {
            arr.add(-8);
        }
        for (int i = 0; i < 3; i++) {
            arr.add(-7);
        }
        for (int i = 0; i < 4; i++) {
            arr.add(-6);
        }
        for (int i = 0; i < 4; i++) {
            arr.add(-5);
        }
        for (int i = 0; i < 4; i++) {
            arr.add(-4);
        }
        for (int i = 0; i < 5; i++) {
            arr.add(-3);
        }
        for (int i = 0; i < 8; i++) {
            arr.add(-2);
        }
        arr.add(-1);
        arr.add(-12);
    }

    // setter
    public void setTable(int[][] table) {
        this.table = table;
    }

    public void setTurn(boolean turn) {
        this.isTurn = turn;
    }

    // getter
    public int[][] getTable() {
        return table;
    }

    public boolean isTurn() {
        return isTurn;
    }
}
