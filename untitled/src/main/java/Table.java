public class Table {
    String[][] Table;

    public void makeTable(int[][] table) {
        Table = new String[21][111];

        for (int i = 0; i < 21; i++) {
            for (int j = 0; j < 111; j++) {

                if (j % 11 == 0) {
                    if (i % 2 == 0) {
                        Table[i][j] = "\u254B";  // +
                    } else {
                        Table[i][j] = "\u2503";
                    }
                } else {
                    if (i % 2 == 0) {
                        Table[i][j] = "\u2501";
                    } else {
                        Table[i][j] = " ";
                    }
                }


                if (i == 0) {
                    if (j % 11 == 0) {
                        Table[i][j] = "\u2533";
                    } else {
                        Table[i][j] = "\u2501";
                    }
                    if (j == 0) {
                        Table[i][j] = "\u250F";
                    }
                    if (j == 110) {
                        Table[i][j] = "\u2513";
                    }
                }

                if (i == 20) {
                    if (j % 11 == 0) {
                        Table[i][j] = "\u253B";
                    } else {
                        Table[i][j] = "\u2501";
                    }
                    if (j == 0) {
                        Table[i][j] = "\u2517";
                    }
                    if (j == 110) {
                        Table[i][j] = "\u251B";
                    }
                }

                if (j == 0) {
                    if (i % 2 == 0 & i != 0 & i != 20) {
                        Table[i][j] = "\u2523";
                    }
                }
                if (j == 110) {
                    if (i % 2 == 0 & i != 0 & i != 20) {
                        Table[i][j] = "\u2528";
                    }
                }
            }
        }


        String piece = "";
        int n = 0;
        for (int i = 0; i < 10; i++) {
            for (int j = 0; j < 10; j++) {
                piece = getPiece(table[i][j]);
                if (table[i][j] < 0) //other pieces are shown with OP
                {
                    Table[i * 2 + 1][j * 11 + 1] = "O";  //OP
                    Table[i * 2 + 1][j * 11 + 2] = "P";
                }
                else if (table[i][j] > 0)
                {
                    for (int a = 0; a < piece.length(); a++) {
                        Table[i * 2 + 1][j * 11 + 1 + a] = "";
                        Table[i * 2 + 1][j * 11 + 1 + a] += piece.charAt(a);
                    }
                }
            }


        }
    }

    static String getPiece (int n){
        if (n < 0) {
            return "Op";
        }
        return switch (n) {
            case 2 -> "Scout";
            case 11 -> "Bomb";
            case 3 -> "Miner";
            case 4 -> "Sergeant";
            case 5 -> "Lieutenant";
            case 6 -> "Captain";
            case 7 -> "Major";
            case 8 -> "Colonel";
            case 9 -> "General";
            case 10-> "Marshal";
            case 1 -> "Spy";
            case 12-> "Flag";
            case 13-> "Island" ;
            default -> " ";
        };
    }


    public void PrintTable()
    {
        for (int i = 0; i < 21; i++) {
            for (int j = 0; j < 111; j++) {
                System.out.print(Table[i][j]);
            }
            System.out.println();
        }
    }
}