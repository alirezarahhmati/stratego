import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;

public class Play {
    public void playGame (Game game) {
        Scanner input = new Scanner(System.in);

        while (!isGameEnd(game.getTable())) {
            // how table and everything
            Table arrTable = new Table();
            arrTable.makeTable(game.getTable());
            arrTable.PrintTable();

            if (game.isTurn()) {
                String data = input.next();

                if (data.equals("v")) {
                    writeInFile(game.getTable());
                }else if (data.equals("e")) {
                    return;
                }else {
                    executeOrder(data, game.getTable());
                    game.setTurn(false);
                }
            } else {
                AI.computerPlay(game.getTable());
                game.setTurn(true);
            }
        }
    }

    public void executeOrder (String data , int[][] table) {
        StringBuilder nut = new StringBuilder();
        StringBuilder row = new StringBuilder();
        StringBuilder col = new StringBuilder();
        StringBuilder order = new StringBuilder();
        int len = data.length();

        for (int i = 0; i < len; i++) {
            if (data.charAt(i) == ',') {
                for (i = i + 1; i < i + 3; i++) {
                    if (data.charAt(i) == ',') {
                        for (i = i + 1; i < i + 3; i++) {
                            if (data.charAt(i) == '-') {
                                for (i = i + 1; i < len; i++) {
                                    order.append(data.charAt(i));
                                }
                                break;
                            }
                            col.append(data.charAt(i));
                        }
                        break;
                    }
                    row.append(data.charAt(i));
                }
                break;
            }
            nut.append(data.charAt(i));
        }

        // error handling
        if (Main.givPower(nut.toString()) == 0) {
            // give message for this error
            System.out.println("Can not move!");
        }
        int intRow = 0;
        int intCol = 0;
        try {
            intRow = 10 - Integer.parseInt(row.toString());
            intCol = Integer.parseInt(col.toString()) - 1;
        } catch (Exception e) {
            // give message for this error
            System.out.println("Can not move!");
        }

        if (nut.toString().equals("Scout")) {
            StringBuilder stringTargetRow = new StringBuilder();
            StringBuilder stringTargetCol = new StringBuilder();

            for (int i = 0; i < 3; i++) {
                if (order.charAt(i) == ',') {
                    for (i = i + 1; i < order.length(); i++) {
                        stringTargetCol.append(order.charAt(i));
                    }
                    break;
                }
                stringTargetRow.append(order.charAt(i));
            }

            int targetRow = 0;
            int targetCol = 0;
            try {
                targetRow = 10 - Integer.parseInt(stringTargetRow.toString());
                targetCol = Integer.parseInt(stringTargetCol.toString()) - 1;
            } catch (Exception e) {
                // give message for this error
                System.out.println("Can not move!");
            }

            if (Move.isScoutMovable(table , intRow , intCol , targetRow , targetCol)) {
                Move.scoutMove(table, intRow, intCol, targetRow, targetCol);
            } else {
                System.out.println("Can not move!");
            }

        } else {
            if (Move.isMovable(table , intRow , intCol , order.charAt(0))) {
                Move.move(table , intRow , intCol , order.charAt(0));
            } else {
                System.out.println("Can not move!");
            }
        }
    }

    public boolean isGameEnd (int[][] table) {
        boolean isCmpNutsEnd = true;
        boolean isUserNutsEnd = true;

        for (int i = 0; i < 10; i++) {
            for (int j = 0; j < 10; j++) {
                if (table[i][j] > 0 && table[i][j] <= 10) {
                    isUserNutsEnd = false;
                }
                if (table[i][j] < 0 && table[i][j] >= -10) {
                    isCmpNutsEnd = false;
                }
                if (!(isCmpNutsEnd && isUserNutsEnd)) {
                    break;
                }
            }
        }
        return isCmpNutsEnd && isUserNutsEnd;
    }

    private void writeInFile(int[][] table) {
        Scanner input = new Scanner(System.in);

        System.out.println("Enter path to save : ");
        String path = input.next();

        File myFile = new File(path);
        try {
            if (!myFile.createNewFile()) {
                System.out.println("This file is already exist.");
                return;
            }

            FileWriter myWriter = new FileWriter(myFile);
            for (int i = 0; i < 10; i++) {
                for (int j = 0; j < 10; j++) {
                    String data = Table.getPiece(table[i][j]);
                    data += ",";
                    data += (10 - i);
                    data += ",";
                    data += (j + 1);
                    data += ("\n");

                    myWriter.write(data);
                }
            }

            myWriter.close();
            System.out.println("Successfully save to the file.");
        } catch (IOException e) {
            System.out.println("An error occurred in creating new File!");
        }
    }
}
